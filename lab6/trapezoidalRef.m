function input = trapezoidalRef(t)
accel_rate=1500;
plateau=600;
start_delay=0.1;
displacement=700;
dead_time=1;
middle_delay=0.1;
time_plat=(displacement-(plateau/accel_rate)*plateau)/plateau;
if t<start_delay
    input=0;
elseif (t<(start_delay+plateau/accel_rate))
    input=(t-start_delay)*accel_rate;
elseif (t<(start_delay+plateau/accel_rate)+time_plat)
    input=plateau;
elseif (t<(start_delay+plateau/accel_rate)+time_plat+plateau/accel_rate);
    input=plateau-accel_rate*(t-start_delay-time_plat-plateau/accel_rate);
elseif (t<(start_delay+plateau/accel_rate)+time_plat+plateau/accel_rate+middle_delay)
    input=0;
elseif (t<(start_delay+plateau/accel_rate)+time_plat+plateau/accel_rate+middle_delay+plateau/accel_rate)
    temp=(start_delay+plateau/accel_rate+time_plat+plateau/accel_rate+middle_delay);
    input =-(t-temp)*accel_rate;
elseif (t<(start_delay+plateau/accel_rate)+time_plat+plateau/accel_rate+middle_delay+plateau/accel_rate+time_plat)
    input =-plateau;
elseif (t<(start_delay+plateau/accel_rate)+time_plat+plateau/accel_rate+middle_delay+plateau/accel_rate+time_plat+plateau/accel_rate)
    input =-plateau+accel_rate*(t-start_delay-time_plat-plateau/accel_rate-middle_delay-plateau/accel_rate-time_plat-plateau/accel_rate);
else
    input=0;
end
end


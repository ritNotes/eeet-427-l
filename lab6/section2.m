s=tf('s');
a=1800.0;
b=6.0;
Gpv=a/(s+b);
Gpp=a/(s*(s+b));

Kp=0.16;
Ki=0.1;
H=1;
Gc=(s*Kp+Ki)/s;

TFv=feedback(Gc*Gpv,H);
figure(3);
step(TFv);
title("Closed Loop Motor Velocity Step Response")

TFp=feedback(Gpp*(Gc),H);
figure(4);
step(TFp);
title("Closed Loop Motor Velocity Step Response")
s=tf('s');
a=1800.0;
b=6.0;
Gpy=a/(s+b);
Gpp=a/(s*(s+b));

figure(1)
step(Gpy);
title("Motor Speed Step Response")
figure(2)
step(Gpp);
title("Motor Position Step Response")
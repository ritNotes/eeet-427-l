s=tf('s');
a=1800.0;
b=6.0;
Gpv=a/(s+b);
Gpp=a/(s*(s+b));

Kp=0.05;
Ki=0.0001;
H=1;
Gc=(s*Kp+Ki)/s;

TFv=feedback(Gc*Gpv,H);

TFp=feedback(Gpp*(Gc),H);

 % Part 3
figure(5);
t=0:0.02:4; % time vector: start, step, stop
vel_ref = arrayfun(@trapezoidalRef,t); % make trapezoidal reference
[pos_ref,t]=lsim(1/s,vel_ref,t); % make position reference
[y,t]=lsim(TFp,pos_ref,t); % linear simulation

plot(t,y)
hold on
plot(t,pos_ref)
title('trapezoidal reference response')
plot(t,vel_ref)
pos_err=pos_ref-y;
plot(t,150*Kp*pos_err)% scale by 50
hold off


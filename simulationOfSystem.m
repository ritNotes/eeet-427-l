clear all;clc;
s=tf('s');
%Motor Position Control System
KpPos=0.2;
a=2491.5;
b=8.333;
Cpos=5.625;
Dpos=20;
Gcpos=KpPos*(Dpos/Cpos)*((s+Cpos)/(s+Dpos));
Gppos=a/(s*(s+b));
Hpos=1;
TFpos=feedback(Gcpos*Gppos,Hpos);

% Ball Position Control System
alpha=5.0/18.0;
KpBallDist=1;
g=9.8;
CBallDist = 1;
DBallDist = 6;
GcDist=(DBallDist/CBallDist)*((s+CBallDist)/(s+DBallDist));
GPlant= (alpha * g)/s^2;
GBallDist = KpBallDist* TFpos * GPlant;%GcDist * 
HBallDist = GcDist;
TFBall=feedback(GBallDist, HBallDist);


%Bode Analysis
figure(1);step(TFBall);grid on;title("Step Response of Closed Loop System");hold on;
figure(2);bode(TFBall);grid on;title("Closed Loop Transfer Function");hold on;
figure(3);bode(GBallDist*HBallDist);grid on;title("Open Loop GH Gain");hold on;

#include <Wire.h>
#include <VL53L1X.h>

uint8_t width, height;
uint8_t center;
boolean firstPrint = true;
int x,y;

VL53L1X sensor;

int i=0;

void setup() {
  Serial.begin(115200);
  Wire.begin();
  Wire.setClock(400000);

  sensor.setTimeout(500);
  if (!sensor.init())
  {
    Serial.println("Failed to detect and initialize sensor!");
    while(1);
  }

  //use long distance mode, allowing for 50000us (50ms) for a measurements. 
  //minimum value for budget is 33ms; 200ms works well
  sensor.setDistanceMode(VL53L1X::Long);
  sensor.setMeasurementTimingBudget(50000);
}

void loop() 
{
  sensor.readSingle(true);
  //printLotsOfStats();
  //printIndexAndPeakSignal();
  printPlottermm();
  delay(10);
  i++;
  if (sensor.timeoutOccurred()) 
  { 
    Serial.print(" TIMEOUT");
  }
}

void printPlottermm()
{
  if(firstPrint)
  {
    Serial.print("Range (mm):");
    Serial.println();
    firstPrint = false;
  }
  Serial.print("\t");
  Serial.print(sensor.ranging_data.range_mm);
  Serial.println();
}

void printIndexandPeakSignal()
{
  if(firstPrint) {
    Serial.print("index:");
    Serial.print("index:");
    Serial.print("index:");
    Serial.print("index:");
    Serial.print("index:");
    Serial.println();
    firstPrint = false;
  }
  Serial.print(i); Serial.print("\t");
  Serial.print(sensor.ranging_data.range_mm); Serial.print("\t"); 
  Serial.print(sensor.ranging_data.peak_signal_count_rate_MCPS); Serial.print("\t"); 
  Serial.print(sensor.rangeStatusToString(sensor.ranging_data.range_status)); Serial.print("\t"); 
  Serial.print(sensor.ranging_data.ambient_count_rate_MCPS);
  Serial.println();
}

void printLotsOfStats()
{
  Serial.print("Index:\t"); Serial.print(i);
  Serial.print("\tRange:"); Serial.print(sensor.ranging_data.range_mm);
  Serial.print("\tPeak:"); Serial.print(sensor.ranging_data.peak_signal_count_rate_MCPS);
  Serial.print("\tAmbient:"); Serial.print(sensor.ranging_data.ambient_count_rate_MCPS);
  Serial.print("\tStatus: "); Serial.print(VL53L1X::rangeStatusToString(sensor.ranging_data.range_status));
  Serial.println();
}

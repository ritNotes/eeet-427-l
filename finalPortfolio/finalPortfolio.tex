%% This is a skeleton file demonstrating the use of IEEEtran.cls
%% (requires IEEEtran.cls version 1.8b or later) with an IEEE
%% journal paper.
%%
%% Support sites:
%% http://www.michaelshell.org/tex/ieeetran/
%% http://www.ctan.org/pkg/ieeetran
%% and
%% http://www.ieee.org/

%%*************************************************************************
%% Legal Notice:
%% This code is offered as-is without any warranty either expressed or
%% implied; without even the implied warranty of MERCHANTABILITY or
%% FITNESS FOR A PARTICULAR PURPOSE!
%% User assumes all risk.
%% In no event shall the IEEE or any contributor to this code be liable for
%% any damages or losses, including, but not limited to, incidental,
%% consequential, or any other damages, resulting from the use or misuse
%% of any information contained here.
%%
%% All comments are the opinions of their respective authors and are not
%% necessarily endorsed by the IEEE.
%%
%% This work is distributed under the LaTeX Project Public License (LPPL)
%% ( http://www.latex-project.org/ ) version 1.3, and may be freely used,
%% distributed and modified. A copy of the LPPL, version 1.3, is included
%% in the base LaTeX documentation of all distributions of LaTeX released
%% 2003/12/01 or later.
%% Retain all contribution notices and credits.
%% ** Modified files should be clearly indicated as such, including  **
%% ** renaming them and changing author support contact information. **
%%*************************************************************************

\documentclass[journal]{IEEEtran}

% *** MATH PACKAGES ***
\usepackage{amsmath}

% *** PDF, URL AND HYPERLINK PACKAGES ***
\usepackage{url}
% correct bad hyphenation here: (DON'T ADD BLANK LINE AFTER THIS COMMAND)
\hyphenation{op-tical net-works semi-conduc-tor}
\usepackage{graphicx}  %needed to include png, eps figures
\usepackage{float}  % used to fix location of images i.e.\begin{figure}[H]

\begin{document}

% paper title
\title{Final Lab Portfolio\\ \small{EEET--427--01: Controls Systems Lab}}

% author names
\author{Blizzard MacDougall, rmm3470}% <-this % stops a space

% The report headers
\markboth{EEET--427--01 Lab Portfolio; Dec. 2021} %Don't remove next line
{Shell}

% make the title area
\maketitle

\begin{abstract}
    In an effort to further understanding of controls systems,
    students were instructed to construct a system capable of balancing a ball on a beam.
    This system was to be able to react to outside stimuli, and keep the ball as stable as possible.
\end{abstract}

\section{Introduction}
\IEEEPARstart{T}{he} purpose of these experiments was to provide real-world experience in implementing the controls systems concepts described in lecture.
In addition, the labs also provided experience with troubleshooting both systems and Arduino code.
Background knowledge necessary for this lab includes basic calculus skills and physics (specifically two-dimensional motion and forces).
These labs make extensive use of an Arduino (purchased at the start of the course), primarily to control a motor.
This is done using code provided by the instructor, which is modified to suit each students particular hardware.
The labs also make use of soldering skills, as well as basic hot-glue and foam-core working skills.
Each portion of the lab received a brief description, and were roughly based off of the work in lecture that week.
Moreover, each lab built upon the previous lab.
The final lab project is pictured in Figure~\ref{fig:finishedBuild}.\\

\section{Parts and Materials}
There are several major components in this lab.
First and foremost, the RoboRED YourDuino board (hereafter referred to as "the Arduino"), which is the microcontroller the labs are based in.
This board controls all of the following devices, either directly or indirectly.
The primary motor used in these labs is the DFRobot \verb|FIT0450| DC motor (hereafter referred to a "the motor").
The ultimate goal of the motor is to control the beam on which a ball will balance.
The motor is controlled by a Seeed studio \verb|MIX1508| H-Bridge Motor Driver (hereafter referred to as "the motor driver" or "the motor driver board").
This motor driver takes power from a steady power source, and controls the motor based on PWM signals sent from the Arduino.
The power for the motor driver in all labs is provided by an AC-to-USB power supply.
In later labs, a Pololu carrier board containing a STMicro \verb|VL53L1X| Time-of-Flight sensor (hereafter referred to as "the TOF sensor" or "the time-of-flight sensor") is used. 
This is used to find the ball's position on the balancing beam.
The lab also makes use of foam core and hot glue, for the balancing beam and for connecting components.
All code for each lab was provided by the professor at the beginning of lab.\\

\newpage
\section{Results}
\subsection{Lab 1}
The first lab's instructions were to create a standard transfer function and block diagram for the motor distributed at the beginning of lab.
This was done by sending a constant voltage to the motor, and graphing its velocity over time, as seen in Figure~\ref{fig:ramp}.
Values of $a$ and $b$ can be found in Table~\ref{table:vars}.
The transfer function for the motor can be represented as $\frac{a}{s+b}$ in Figure~\ref{fig:block}, and is calculated in Equation~\ref{calculation:motorBlock}.
These values are key for all following labs, as every subsequent lab's code uses these values to control the motor.

\subsection{Lab 2}
This lab's purpose was to compare different ways of controlling the motor.
This was done by giving the motor a trapezoidal reference signal, and comparing the response with different control schemes.
The control schemes tested in this lab were using closed-loop feedback and feed-forward.
The graph in Figure~\ref{fig:loopFF} shows the final comparison of all of these control mechanisms.
This lab found that using both closed-loop feedback and feed-forward in conjunction gave the fastest response time with the most accurate steady-state speed.

\subsection{Lab 3}
The purpose of this lab was to introduce how to decouple the input, and how to ensure that the motor works properly when programmed to turn backwards.
This used an expanded version of the trapezoidal reference signal used in the previous lab, including a negative portion of the signal.
Figure~\ref{fig:decouple} shows both the input reference signal, and that the output signal is close to the same path.
In addition, this lab ensured that the motor never tried to use more power than was available (in this case, $5V$). 
This can be seen in the yellow line in Figure~\ref{fig:decouple}.

\subsection{Lab 4}
This lab's instructions were to take the previous control schemes, and add integral control on top of them. 
This used the same trapezoidal reference signal as in Lab 3.
This resulted in a motor response curve that was more accurate to the input signal, and was more accurate in position. 
Using the control system consisting only of proportional control and feed-forward produced an error in speed of $35.49rad/sec$,
while the control system including integral control produced an error of only $11.82rad/sec$.
This can be seen in comparing Figure~\ref{fig:decouple} and Figure~\ref{fig:PI}, and comparing each \verb|motorspeed| plot with the \verb|refVel| or \verb|Wref| plots.

\subsection{Lab 5}
In this lab, students moved from tuning the velocity error to tuning the position error.
This is crucial, as a high error in the position of the motor will mean an even higher error in the beam's position, making it impossible to balance the ball properly.
To do this, the control system was given a constant position, and the motor's position over time was plotted.
After tuning, the motor's error in position was reduced to $1.20rad$, with a response time of $<500ms$, as shown in Figure~\ref{fig:pos}.

\section{Discussion and Summary}
%%Yes, I know, there's a break in writing style here. Its to fill a requirement and hopefully pass the course.
There were some errors in the directions provided by the professor.
This resulted in some uncertainty as to how to proceed, the most glaring of which was a discrepancy between 
the completed system the professor would periodically demonstrate and the specifications for correctly building the system.
More specifically, the length of the beam on which the ball sits was significantly longer than the base that supported it.
To solve this issue, I shortened the beam to fit the base. 
This resulted in needing to modify most of the code given by the professor, to account for the differing ratio between beam length 
and the range of motion of the motor.\\

A majority of the time, I was working outside of my comfort zone, as the last time I had worked with Arduino code previous to this lab was almost two years ago. 
This encouraged me to relearn C++ outside of class, and pushed me to learn more languages in preparation for the future to mitigate problems caused by inexperience.\\

Most of my physical mechanical skills came in handy in this course.
Particularly, small arts-and-crafts skills like use of foam core and hot glue.
This helped when it came time to troubleshoot the mis-sized beam, as I not only had to change the length of the beam itself, 
but also account for the beam in the lifting arm. 
This can be seen in Figure~\ref{fig:finishedBuild} in the triangular notch cut out of the lifting arm on the right side of the device, allowing full range of motion for the motor.\\

Overall, I learned that I find errors I've made by helping others.
There were several times in the process of physically building the system that I had made some measurement incorrectly, particularly the beam length miscalculation mentioned previously.
Only by other students asking me questions about their system did I realize I made the measurement error.
Overall, I've found that I tend to work best in a small group of people, where we collectively share information back to the larger group.\\

By the end of this course, I had a good grasp on most first-order system concepts. 
In addition, I also understood how to control said systems, both mathematically and in Arduino code.

\newpage
\appendices
\section{Equations and Calculations}
\subsection{Equations}
\begin{equation}
    \frac1b=\tau=t_{65\%steady-state}
    \label{equation:timeConstant}
\end{equation}

\begin{equation}
    \begin{split}
        \omega_{mtr;steady-state}\approx750rad/s\\
        V_{arm}=2.5V\\
        \frac{\omega_{mtr}}{V_{arm}}\approx299\frac{rad}{sV}\\
        0.65*\omega_{mtr;steady-state}=450rad/s\\
        t_{65\%steady-state}\approx120ms\\
        b=\frac{1}{\tau}=\frac{1}{0.12s}=8.\overline3Hz\\
        \frac{a}{s+b}@\ steady-state=\frac ab\\
        299\frac{rad}{sV}=\frac{a}{b}\\
        a=\left(299\frac{rad}{sV}\right)(8.\overline3Hz)\\
        a=2491.5\frac{rad}{s^2V}
    \end{split}
    \label{calculation:motorBlock}
\end{equation}

\section{Tables}
\begin{table}[!ht] %[H]
\centering
\label{table:vars}
\begin{tabular}{c|r|l}
Variable &  value & unit\\ \hline
$a$ & $2491.5$ & $\frac{rad}{s^2V}$\\\hline
$b$ & $9.\overline{3}$ & $Hz$\\\hline
$g$ & $9.81$ & $m/s$\\\hline
$L_{arm}$ & $10$ & $cm$\\\hline
$L_{beam}$ & $36$ & $cm$\\\hline
$C_{beam}$ & $5.625$\\\hline
$D_{beam}$ & $20$\\\hline
$C_{ball}$ & $1$\\\hline
$D_{ball}$ & $6$\\\hline
$K_{p,arm}$ & $0.1$ & $\frac{Vs}{rad}$\\\hline
$K_{p,ball}$ & $1$ & $\frac{rad}{m}$\\
\end{tabular}
\caption{Values for variables in Figure~\ref{fig:block}}
\end{table}

\section{Images}
\begin{figure}[H]%[!ht]
\begin {center}
    \includegraphics[width=0.45\textwidth]{constructedSystem.jpg}
    \caption{The completed physical system.}
\label{fig:finishedBuild}
\end{center}
\end{figure}

\begin{figure}[H]%[!ht]
\begin {center}
    \includegraphics[width=0.45\textwidth]{motorSpeedRamp.png}
    \caption{Motor response to a steady 2.5V input}
\label{fig:ramp}
\end{center}
\end{figure}

\begin{figure}[H]%[!ht]
\begin {center}
    \includegraphics[width=0.45\textwidth]{lab2FinalGraph.png}
    \caption{Comparison of open-loop, closed-loop, and feed-forward systems.}
\label{fig:loopFF}
\end {center}
\end{figure}

\begin{figure}[H]%[!ht]
\begin {center}
    \includegraphics[width=0.45\textwidth]{lab3VelocityGraph.png}
    \caption{Trapezoidal reference signal input and output with proportional control.}
\label{fig:decouple}
\end {center}
\end{figure}

\begin{figure}[H]%[!ht]
\begin {center}
    \includegraphics[width=0.45\textwidth]{lab4TunedPlot.png}
    \caption{Trapezoidal reference signal with PI control.}
\label{fig:PI}
\end {center}
\end{figure}

\begin{figure}[H]%[!ht]
\begin {center}
    \includegraphics[width=0.45\textwidth]{lab5PositionTunedPlot.png}
    \caption{Static position response curve for the motor.}
\label{fig:pos}
\end {center}
\end{figure}

\begin{figure}[H]%[!ht]
\begin {center}
    \includegraphics[width=0.45\textwidth]{completeSystemBlockDiagram.jpg}
    \caption{General block diagram for a Ball-On-Beam system}
\label{fig:block}
\end {center}
\end{figure}

\end{document}
